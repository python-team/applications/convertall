<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_DE">
<context>
    <name>cmdline</name>
    <message>
        <location filename="cmdline.py" line="22"/>
        <source>Usage:</source>
        <translation>Aufruf:</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="24"/>
        <source>qt-options</source>
        <translation>Qt-Optionen</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="32"/>
        <source>-or- (non-GUI):</source>
        <translation>oder (nicht GUI):</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="33"/>
        <source>options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="28"/>
        <source>number</source>
        <translation>Zahl</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="29"/>
        <source>from_unit</source>
        <translation>Ausgangseinheit</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="30"/>
        <source>to_unit</source>
        <translation>Zieleinheit</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="35"/>
        <source>Units with spaces must be &quot;quoted&quot;</source>
        <translation>Einheiten mit Leerzeichen müssen &quot;gequotet&quot; werden</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="37"/>
        <source>Options:</source>
        <translation>Optionen:</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="38"/>
        <source>num</source>
        <translation>zahl</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="39"/>
        <source>set number of decimals to show</source>
        <translation>Anzahl der Nachkommastellen festlegen</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="41"/>
        <source>show set number of decimals, even if zeros</source>
        <translation>Die angegebene Anzahl Dezimalstellen auch für Null anzeigen</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="47"/>
        <source>display this message and exit</source>
        <translation>Diesen Text anzeigen und beenden</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="49"/>
        <source>interactive command line mode (non-GUI)</source>
        <translation>Interaktiver Kommandozeilenmodus (ohne GUI)</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="51"/>
        <source>convert without further prompts</source>
        <translation>Ohne weitere Eingaben konvertieren</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="43"/>
        <source>show results in scientific notation</source>
        <translation>Ergebnis in wissenschaftlicher Schreibweise anzeigen</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="116"/>
        <source>Enter from unit -&gt; </source>
        <translation>Ausgangseinheit angeben -&gt; </translation>
    </message>
    <message>
        <location filename="cmdline.py" line="122"/>
        <source>Enter to unit -&gt; </source>
        <translation>Zieleinheit angeben -&gt; </translation>
    </message>
    <message>
        <location filename="cmdline.py" line="139"/>
        <source>Enter number, [n]ew, [r]everse or [q]uit -&gt; </source>
        <translation>Zahl eingeben, [n]eu, umgekeh[r]t, [q]uittieren -&gt; </translation>
    </message>
    <message>
        <location filename="cmdline.py" line="157"/>
        <source>Units {0} and {1} are not compatible</source>
        <translation>Einheiten sind nicht kompatibel ({0} bzw. {1})</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="173"/>
        <source>{0} is not a valid unit</source>
        <translation>{0} ist keine gültige Einheit</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="45"/>
        <source>show results in engineering notation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>convertdlg</name>
    <message>
        <location filename="convertdlg.py" line="64"/>
        <source>%d units loaded</source>
        <translation type="obsolete">%d Einheiten geladen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="67"/>
        <source>Error in unit data - %s</source>
        <translation type="obsolete">Fehler in Einheitendaten - %s</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="85"/>
        <source>From Unit</source>
        <translation>Ausgangseinheit</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="100"/>
        <source>To Unit</source>
        <translation>Zieleinheit</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="117"/>
        <source>Set units</source>
        <translation>Einheit angeben</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="134"/>
        <source>No Unit Set</source>
        <translation>Keine Einheit angegeben</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="152"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="156"/>
        <source>&amp;Unit Finder...</source>
        <translation>Einheit s&amp;uchen...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="160"/>
        <source>&amp;Options...</source>
        <translation>&amp;Optionen...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="164"/>
        <source>&amp;Help...</source>
        <translation>&amp;Hilfe...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="168"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="207"/>
        <source>Clear Unit</source>
        <translation>Einheit löschen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="283"/>
        <source>Result Display</source>
        <translation>Ergebnis-Ausgabe</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="280"/>
        <source>Decimal places</source>
        <translation>Dezimalstellen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="286"/>
        <source>Use scientific notation</source>
        <translation>Wissenschaftliche Darstellung nutzen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="285"/>
        <source>Use fixed decimal places</source>
        <translation>Festkommadarstellung nutzen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="277"/>
        <source>Buttons</source>
        <translation type="obsolete">Schaltflächen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="294"/>
        <source>Show operator buttons</source>
        <translation>Schaltflächen für Rechenoperationen anzeigen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="297"/>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="299"/>
        <source>Use default system colors</source>
        <translation>Systemfarben nutzen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="300"/>
        <source>Set background color</source>
        <translation>Hintergrundfarbe setzen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="301"/>
        <source>Set text color</source>
        <translation>Textfarbe setzen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="379"/>
        <source>Read Me file not found</source>
        <translation>README-Datei nicht gefunden</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="382"/>
        <source>ConvertAll README File</source>
        <translation>ConvertAll-README-Datei</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="365"/>
        <source>ConvertAll Version %s
by %s</source>
        <translation type="obsolete">ConvertAll Version %s
von %s</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="216"/>
        <source>Recent Unit</source>
        <translation>Zuletzt benutzte Einheit</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="288"/>
        <source>Recent Units</source>
        <translation>Zuletzt benutzte Einheiten</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="289"/>
        <source>Number saved</source>
        <translation>Gespeicherte Anzahl</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="68"/>
        <source>Error in unit data - {0}</source>
        <translation>Fehler in Einheitendaten - {0}</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="71"/>
        <source>{0} units loaded</source>
        <translation>{0} Einheiten geladen</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="291"/>
        <source>Load last units at startup</source>
        <translation type="unfinished">Laden der letzten Einheiten beim Start</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="292"/>
        <source>User Interface</source>
        <translation>User Interface</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="296"/>
        <source>Show tip at startup</source>
        <translation type="unfinished">Zeige Hinweise beim Start</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="390"/>
        <source>ConvertAll Version {0}
by {1}</source>
        <translation>ConvertAll Version {0}
von {1}</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="418"/>
        <source>Convertall - Tip</source>
        <translation>Convertall - Hinweis</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="444"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="422"/>
        <source>Combining Units</source>
        <translation type="unfinished">Die Kombination von Einheiten</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="431"/>
        <source>&lt;p&gt;ConvertAll&apos;s strength is the ability to combine units:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Enter &quot;m/s&quot; to get meters per second&lt;/li&gt;&lt;li&gt;Enter &quot;ft*lbf&quot; to get foot-pounds (torque)&lt;/li&gt;&lt;li&gt;Enter &quot;in^2&quot; to get square inches&lt;/li&gt;&lt;li&gt;Enter &quot;m^3&quot; to get cubic meters&lt;/li&gt;&lt;li&gt;or any other combinations you can imagine&lt;/li&gt;</source>
        <translation type="obsolete">&lt;p&gt;ConvertAll Stärke ist die Fähigkeit, Geräte zu kombinieren:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Geben Sie &quot;m / s&quot; in Meter pro Sekunde zu bekommen&lt;/li&gt;&lt;li&gt;geben Sie &quot;ft · lbf&quot;, um Fuß-Pfund (Drehmoment) zu erhalten&lt;li&gt;&lt;li&gt;Enter &quot;in ^ 2&quot; auf Platz Zoll zu bekommen&lt;/li&gt;&lt;li&gt;geben Sie &quot;m ^ 3 &quot;, um Kubikmeter&lt;/li&lt;li&gt;oder andere Kombinationen, die Sie sich vorstellen können zu&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="439"/>
        <source>Show this tip at startup</source>
        <translation type="unfinished">Zeige diesen Hinweis beim Start</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="434"/>
        <source>&lt;p&gt;ConvertAll&apos;s strength is the ability to combine units:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Enter &quot;m/s&quot; to get meters per second&lt;/li&gt;&lt;li&gt;Enter &quot;ft*lbf&quot; to get foot-pounds (torque)&lt;/li&gt;&lt;li&gt;Enter &quot;in^2&quot; to get square inches&lt;/li&gt;&lt;li&gt;Enter &quot;m^3&quot; to get cubic meters&lt;/li&gt;&lt;li&gt;or any other combinations you can imagine&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished">&lt;p&gt;Die Stärke von ConvertAll ist die Fähigkeit, Einheiten zu kombinieren:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Geben Sie &quot;m / s&quot; ein, um Meter pro Sekunde zu bekommen&lt;/li&gt;&lt;li&gt;Geben Sie &quot;ft · lbf&quot; ein, um Fuß-Pfund (Drehmoment) zu bekommen&lt;li&gt;&lt;li&gt;Geben Sie &quot;in ^ 2&quot; ein, um Quadratzoll zu bekommen&lt;/li&gt;&lt;li&gt;Geben Sie &quot;m ^ 3 &quot; ein, um Kubikmeter zu bekommen&lt;/li&gt;&lt;li&gt;oder jede andere Kombinationen, die Sie sich vorstellen können&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="279"/>
        <source>Result Precision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="284"/>
        <source>Use short representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="287"/>
        <source>Use engineering notation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>finddlg</name>
    <message>
        <location filename="finddlg.py" line="28"/>
        <source>Unit Finder</source>
        <translation>Einheitensuche</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="35"/>
        <source>&amp;Filter Unit Types</source>
        <translation>Einheitentypen &amp;filtern</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="41"/>
        <source>&amp;Search String</source>
        <translation>&amp;Suchtext</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="46"/>
        <source>C&amp;lear</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="55"/>
        <source>From Unit</source>
        <translation>Ausgangseinheit</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="58"/>
        <source>&amp;Replace</source>
        <translation>&amp;Ersetzen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="61"/>
        <source>&amp;Insert</source>
        <translation>E&amp;infügen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="64"/>
        <source>To Unit</source>
        <translation>Zieleinheit</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="67"/>
        <source>Replac&amp;e</source>
        <translation>E&amp;rsetzen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="70"/>
        <source>Inser&amp;t</source>
        <translation>Ein&amp;fügen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="76"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Unit Name</source>
        <translation>Einheitenname</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Unit Type</source>
        <translation>Einheitentyp</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="135"/>
        <source>[All]</source>
        <translation>[Alle]</translation>
    </message>
</context>
<context>
    <name>helpview</name>
    <message>
        <location filename="helpview.py" line="43"/>
        <source>&amp;Back</source>
        <translation>&amp;Zurück</translation>
    </message>
    <message>
        <location filename="helpview.py" line="51"/>
        <source>&amp;Forward</source>
        <translation>&amp;Weiter</translation>
    </message>
    <message>
        <location filename="helpview.py" line="59"/>
        <source>&amp;Home</source>
        <translation>Zum &amp;Anfang</translation>
    </message>
    <message>
        <location filename="helpview.py" line="67"/>
        <source>Find</source>
        <translation> Suchen</translation>
    </message>
    <message>
        <location filename="helpview.py" line="74"/>
        <source>Find &amp;Previous</source>
        <translation>&amp;Rückwärts Suchen</translation>
    </message>
    <message>
        <location filename="helpview.py" line="81"/>
        <source>Find &amp;Next</source>
        <translation>&amp;Vorwärts Suchen</translation>
    </message>
    <message>
        <location filename="helpview.py" line="114"/>
        <source>Text string not found</source>
        <translation>String nicht gefunden</translation>
    </message>
</context>
<context>
    <name>numedit</name>
    <message>
        <location filename="numedit.py" line="92"/>
        <source>Error in unit data - %s</source>
        <translation type="obsolete">Fehler in Einheitendaten - %s</translation>
    </message>
    <message>
        <location filename="numedit.py" line="57"/>
        <source>Converting...</source>
        <translation>Konvertiere...</translation>
    </message>
    <message>
        <location filename="numedit.py" line="66"/>
        <source>Units are not compatible (%s  vs.  %s)</source>
        <translation type="obsolete">Einheiten sind nicht kompatibel (%s bzw. %s)</translation>
    </message>
    <message>
        <location filename="numedit.py" line="76"/>
        <source>Set units</source>
        <translation>Einheiten angeben</translation>
    </message>
    <message>
        <location filename="numedit.py" line="77"/>
        <source>No Unit Set</source>
        <translation>Keine Einheit angegeben</translation>
    </message>
    <message>
        <location filename="numedit.py" line="102"/>
        <source>Error in unit data - {0}</source>
        <translation>Fehler in Einheitendaten - {0}</translation>
    </message>
    <message>
        <location filename="numedit.py" line="70"/>
        <source>Units are not compatible ({0}  vs.  {1})</source>
        <translation>Einheiten sind nicht kompatibel ({0} bzw. {1})</translation>
    </message>
</context>
<context>
    <name>optiondlg</name>
    <message>
        <location filename="optiondlg.py" line="38"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="optiondlg.py" line="41"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="optiondlg.py" line="44"/>
        <source>Preferences</source>
        <translation>Voreinstellungen</translation>
    </message>
</context>
<context>
    <name>unitatom</name>
    <message>
        <location filename="unitatom.py" line="45"/>
        <source>Bad equation for &quot;%s&quot;</source>
        <translation type="obsolete">Ungültige Gleichung für &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="unitatom.py" line="45"/>
        <source>Bad equation for &quot;{0}&quot;</source>
        <translation>Ungültige Gleichung für &quot;{0}&quot;</translation>
    </message>
</context>
<context>
    <name>unitdata</name>
    <message>
        <location filename="unitdata.py" line="60"/>
        <source>Can not read &quot;units.dat&quot; file</source>
        <translation>Kann Datei &quot;units.dat&quot; nicht lesen</translation>
    </message>
    <message>
        <location filename="unitdata.py" line="86"/>
        <source>Duplicate unit names found</source>
        <translation>Einheitennamen mehrfach vorhanden</translation>
    </message>
</context>
<context>
    <name>unitgroup</name>
    <message>
        <location filename="unitgroup.py" line="302"/>
        <source>Circular unit definition</source>
        <translation>Einheitendefinition enthält Selbstbezug</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="286"/>
        <source>Invalid conversion for &quot;%s&quot;</source>
        <translation type="obsolete">Ungültige Umwandlung für &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="356"/>
        <source>Cannot combine non-linear units</source>
        <translation>Kann keine nichtlinearen Einheiten kombinieren</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="368"/>
        <source>Bad equation for %s</source>
        <translation type="obsolete">Ungültige Gleichung für %s</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="307"/>
        <source>Invalid conversion for &quot;{0}&quot;</source>
        <translation>Ungültige Umwandlung für &quot;{0}&quot;</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="395"/>
        <source>Bad equation for {0}</source>
        <translation>Ungültige Gleichung für {0}</translation>
    </message>
</context>
<context>
    <name>unitlistview</name>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Unit Name</source>
        <translation>Einheitenname</translation>
    </message>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Unit Type</source>
        <translation>Einheitentyp</translation>
    </message>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
</context>
</TS>
